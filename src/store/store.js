import Vue from "vue";
import Vuex from "vuex";
import axios from 'axios';
import { WEATHER_URL, APP_ID } from '../constans';

Vue.use(Vuex);


let cities = localStorage.getItem('recentSearches');



export default new Vuex.Store({
    state: {
        cities: cities ? JSON.parse(cities) : [],
        units: "metric",
        weather: {
            city: null,
            temp: null,
            windSpeed: null,
            pressure: null,
            img: null,
            max_temp: null,
            min_temp: null,
            country: null
        },
    },
    mutations: {
        setWeather(state, weather) {
            state.weather = weather;
        },
        setCity(state, city) {
            state.weather.city = city;
            localStorage.city = city;
        },
        addCity(state, data) {
            const cityInList = state.cities.find((i) => i === data);
            if (!cityInList) {
                state.cities.push(data);
                this.commit('saveCity')
            }
            else return;
        },
        saveCity(state) {
            localStorage.setItem('recentSearches', JSON.stringify(state.cities));
        },
        removeCity(state, item) {
            let index = state.cities.indexOf(item);
            if (index > -1) {
                state.cities.splice(index, 1);
            }
            localStorage.setItem('recentSearches', JSON.stringify(state.cities));
        },
        searchCity(state, elem) {
            state.cities.forEach(element => {
                element == elem
            });
        },
    },
    actions: {
        postWeather(context, city) {
            axios.get(WEATHER_URL + "&q=" + city + "&units=" + this.state.units).then(response => {
                context.commit("setCity", response.data.name);
                context.commit("setWeather", {
                    city: response.data.name,
                    temp: Math.round(response.data.main.temp),
                    windSpeed: response.data.wind.speed,
                    pressure: response.data.main.pressure,
                    img: response.data.weather[0].icon,
                    min_temp: Math.round(response.data.main.temp_min),
                    max_temp: Math.round(response.data.main.temp_max),
                    country: response.data.sys.country
                });
            })
        },
        addtoList(context, product) {
            context.commit("addCity", product)
        },
        getWeather(context, city) {
            axios.get(WEATHER_URL + "&q=" + city + "&units=" + this.state.units).then(response => {
                context.commit('searchCity', response.data.name)
                context.commit("setWeather", {
                    city: response.data.name,
                    temp: Math.round(response.data.main.temp),
                    windSpeed: response.data.wind.speed,
                    pressure: response.data.main.pressure,
                    img: response.data.weather[0].icon,
                    min_temp: Math.round(response.data.main.temp_min),
                    max_temp: Math.round(response.data.main.temp_max),
                    country: response.data.sys.country
                });
                localStorage.city = response.data.name;
            })
        },
        getCoordsData(context, pos) {
            let lat = pos.lat;
            let lon = pos.lon;
            axios.get('https://api.openweathermap.org/data/2.5/weather' + '?lat=' + lat + '&lon=' + lon + "&units=" + this.state.units + '&' + APP_ID).then(response => {
                context.commit("setCity", response.data.name);
                context.commit("setWeather", {
                    city: response.data.name,
                    temp: Math.round(response.data.main.temp),
                    windSpeed: response.data.wind.speed,
                    pressure: response.data.main.pressure,
                    img: response.data.weather[0].icon,
                    min_temp: Math.round(response.data.main.temp_min),
                    max_temp: Math.round(response.data.main.temp_max),
                    country: response.data.sys.country
                });
                localStorage.city = response.data.name;
            })
        },
    },
    getters: {
        weather: state => state.weather,
        cities: state => state.cities
    }
})
