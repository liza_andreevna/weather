import Vue from 'vue'
import App from './App'
import router from './router'
import store from "./store/store";
import { library } from '@fortawesome/fontawesome-svg-core'
import { faArrowUp, faArrowDown, faMapMarkerAlt, faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


library.add(faArrowUp,faArrowDown,faMapMarkerAlt,faSearch)
 
Vue.component('font-awesome-icon', FontAwesomeIcon)



Vue.config.productionTip = false


new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
