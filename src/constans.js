const APP_ID = "appid=cd35194d13eaa3279819f087d4480162";
const WEATHER_URL = `https://api.openweathermap.org/data/2.5/weather?${APP_ID}`;
const GEO_KEY = '385aaa4f-7ced-463f-980e-c2cfcdfa20e9';

export { APP_ID, WEATHER_URL, GEO_KEY};