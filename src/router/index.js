import Vue from 'vue'
import Router from 'vue-router'
import WeatherCard from '@/components/WeatherCard'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: WeatherCard
    }
  ]
})
